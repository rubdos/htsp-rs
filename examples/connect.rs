extern crate htsp;
extern crate tokio_core;
extern crate futures;
extern crate env_logger;

use std::{env,io};
use std::sync::RwLock;

use futures::{Future, Stream};
use tokio_core::reactor::Core;

use htsp::{Metadata, MetadataManager, MetadataManagerFuture};

fn main() {
    env_logger::init().unwrap();
    let addr = env::args().skip(1).next().unwrap_or("127.0.0.1:9982".into()).parse().unwrap();
    let username = env::args().skip(2).next().unwrap_or("kodi".into());
    let password = env::args().skip(3).next().unwrap_or("kodi".into());

    let mut core = Core::new().unwrap();
    let handle = core.handle();
    let connector =
        htsp::Connection::connect(&addr, &handle.clone())
        .and_then(|connection| {
            println!("TCP Connected! Saying hello");
            connection.hello("Connect demo".into(), "0.1.0".into())
        })
        .and_then(|connection| {
            println!("Response from connection:");
            println!(" Negotiated version: {}", connection.version().unwrap());
            println!(" Server name: {} {}", connection.server_name().unwrap(),
                                            connection.server_version().unwrap());
            connection.authenticate(&username, &password)
        })
        .map_err(|err| {
            println!("Could not authenticate as {}", username);
            err
        });

    let mut connection = core.run(connector).unwrap();

    println!("Successfully authenticated as {}", username);
    println!("Requesting metadata");

    let metadata_manager: MetadataManagerFuture<_> = connection.subscribe_metadata().into();

    let (metadata_manager, metadata_stream) = core.run(metadata_manager).unwrap();
    println!("Initial metadata sync complete :-)");

    for channel in &metadata_manager.channels {
        println!("Channel: {:?}", channel);
    }

    let metadata_manager = RwLock::new(metadata_manager);
    let metadata_updater = metadata_stream.for_each(|meta| {
        let mut man = metadata_manager.write()
            .expect("Oh noes. metadata_manager was poisoned");
        man.update(meta);
        Ok(())
    });

    core.run(metadata_updater).unwrap();

    for channel in &metadata_manager.read().unwrap().channels {
        println!("Channel: {:?}", channel.name);
    }
}
