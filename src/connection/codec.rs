use std::io;
use bytes::{BytesMut, BufMut, ByteOrder, BigEndian};

use tokio_io::{AsyncRead, AsyncWrite};
use tokio_io::codec::{Decoder, Encoder};
use tokio_proto::multiplex::RequestId;

use super::proto::{HtspFrame, HtspProto};
use ::message::{HtsMsgField, MapMessage};

pub struct HtspCodec;

impl Decoder for HtspCodec {
    type Item = HtspFrame;
    type Error = io::Error;

    fn decode(&mut self, buf: &mut BytesMut) -> io::Result<Option<Self::Item>> {
        debug!("Trying to decode buffer, with {} bytes in it", buf.len());
        if buf.len() < 4 {
            // Impossible to decode anything
            return Ok(None)
        }
        let len = BigEndian::read_u32(&buf) as usize;
        if len > buf.len() - 4 {
            return Ok(None);
        }
        buf.split_to(4);
        trace!("Decoding message of {} bytes", len);
        if len  == 0 {
            return Ok(None);
        }
        let mut buf = buf.split_to(len);

        MapMessage::decode(&mut buf).and_then(|v| {
            match v.get("seq".into()) {
                Some(&HtsMsgField::S64(i)) => Ok(Some(HtspFrame::RpcResponse(i as u64, v))),
                Some(_) => Err(io::Error::from(io::ErrorKind::InvalidData)),
                None => {
                    match v.get("method".into()).and_then(|a| match a {
                        &HtsMsgField::Str(ref method) => {
                            Some(method.clone())
                        },
                        _ => None
                    }) {
                        Some(method) => {
                            Ok(Some(HtspFrame::ProcedureCall(method.clone(), v)))
                        }
                        _ => {
                            error!("Invalid data. No method nor seq set.");
                            Err(io::Error::from(io::ErrorKind::InvalidData))
                        }
                    }
                }
            }
        })
    }
}

impl Encoder for HtspCodec {
    type Item = (RequestId, MapMessage);
    type Error = io::Error;

    fn encode(&mut self, (id, mut msg): Self::Item, buf: &mut BytesMut) -> io::Result<()> {
        msg.insert("seq".into(), (id as i64).into());
        let length_index = buf.len();

        buf.reserve(4);
        buf.put_u32::<BigEndian>(0); // Placeholder
        try!(msg.encode(buf));
        let length = buf.len() - (length_index + 4);
        buf.reserve(4);
        BigEndian::write_u32(&mut buf[length_index..length_index+4],
                                length as u32);
        debug!("Wrote message of length {}", length);
        Ok(())
    }
}

#[cfg(test)]
#[test]
fn decode_error() {
    let mut state = HtspCodec;

    let mut b = BytesMut::from(vec![
        0x00, 0x00, 0x00, 0x25, 0x03, 0x05, 0x00, 0x00,
        0x00, 0x11, 0x65, 0x72, 0x72, 0x6f, 0x72, 0x49,
        0x6e, 0x76, 0x61, 0x6c, 0x69, 0x64, 0x20, 0x61,
        0x72, 0x67, 0x75, 0x6d, 0x65, 0x6e, 0x74, 0x73,
        0x02, 0x03, 0x00, 0x00, 0x00, 0x00, 0x73, 0x65,
        0x71]);
    let (id, msg) = match state.decode(&mut b).unwrap().unwrap() {
        HtspFrame::RpcResponse(a, b) => (a, b),
        _ => unreachable!(),
    };
    match msg.get("error".into()) {
        Some(&HtsMsgField::Str(ref msg)) => {
            assert_eq!(msg, "Invalid arguments");
        },
        _ => unreachable!()
    }
}
