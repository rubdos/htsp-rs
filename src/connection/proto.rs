use std::{self, io};
use std::collections::HashMap;

use futures::{Async, AsyncSink, Future, IntoFuture, Stream, Sink, Poll};
use futures::sync::oneshot;
use futures::sync::mpsc;
use futures::future::Executor;

use tokio_core::reactor::Handle;
use tokio_io::{AsyncRead, AsyncWrite};
use tokio_io::codec::Framed;
use tokio_proto::BindClient;
use tokio_proto::multiplex::RequestId;
use tokio_proto::util::client_proxy::{self, ClientProxy, Receiver};
use tokio_proto::streaming::multiplex::advanced::Multiplex;

use ::message::{HtsMsgField, MapMessage};
use ::metadata::Metadata;

use super::codec::HtspCodec;
use super::service::HtspService;

pub struct HtspProto;

pub struct HtspProtocolKind;
impl<T: AsyncRead + AsyncWrite + 'static> BindClient<HtspProtocolKind, T> for HtspProto {
    type ServiceRequest = MapMessage;
    type ServiceResponse = MapMessage;
    type ServiceError = io::Error;
    type BindClient = HtspService;

    fn bind_client(&self, handle: &Handle, io: T) -> Self::BindClient {
        let transport = io.framed(HtspCodec);

        let (client, rx) = client_proxy::pair();
        let (metadata_tx, metadata_rx) = mpsc::channel(1);

        let task = Dispatch {
            transport: transport,
            requests: rx,
            in_flight: HashMap::new(),
            next_id: 0,
            metadata_requests: metadata_rx,
            metadata_subscriptions: vec![],
        }.map_err(|e| {
            error!("Dispatch threw an error: {:?}", e);
        });
        handle.execute(task)
            .expect("Failed to spawn dispatch");

        HtspService::new(client, handle, metadata_tx)
    }
}

pub enum HtspFrame {
    RpcResponse(RequestId, MapMessage),
    ProcedureCall(String, MapMessage),
}

pub struct Dispatch<T>
    where T: Stream + Sink
{
    transport: T,
    requests: Receiver<MapMessage, MapMessage, io::Error>,
    in_flight: HashMap<RequestId, oneshot::Sender<Result<MapMessage, io::Error>>>,
    next_id: RequestId,
    metadata_requests: mpsc::Receiver<mpsc::Sender<Metadata>>,
    metadata_subscriptions: Vec<mpsc::Sender<Metadata>>,
}

impl<T> Dispatch<T>
    where T: Stream<Item=HtspFrame, Error=io::Error>
           + Sink<SinkItem=(RequestId, MapMessage), SinkError=io::Error>
{
    fn dispatch(&mut self, frame: HtspFrame) -> io::Result<()> {
        trace!("Dispatch::dispatch");
        match frame {
            HtspFrame::RpcResponse(key, msg) => {
                self.dispatch_rpc(key, msg)
            },
            HtspFrame::ProcedureCall(method, msg) => {
                match method.as_ref() {
                    "tagAdd"
                        |"channelAdd"
                        |"tagUpdate"
                        |"dvrEntryAdd"
                        |"dvrEntryUpdate"
                        |"eventAdd"
                        |"initialSyncCompleted" => {
                            self.dispatch_metadata(try!(msg.into()))
                    }
                    _ => {
                        error!("Unknown method {}", method);
                        Err(io::Error::from(io::ErrorKind::InvalidData))
                    }
                }
            }
        }
    }

    fn dispatch_rpc(&mut self, key: RequestId, msg: MapMessage) -> io::Result<()> {
        self.in_flight.remove(&key).ok_or(io::Error::from(io::ErrorKind::InvalidData))?
            .send(Ok(msg)).map_err(|_| io::Error::from(io::ErrorKind::UnexpectedEof))
    }

    fn dispatch_metadata(&mut self, msg: Metadata) -> io::Result<()> {
        for mut subscription in self.metadata_subscriptions.iter_mut() {
            // TODO: correctly caching and polling those channels
            match subscription.start_send(msg.clone()) {
                Ok(AsyncSink::Ready) => (),
                Ok(AsyncSink::NotReady(msg)) => {
                    trace!("   --> not ready");

                    return Err(io::Error::from(io::ErrorKind::Other));
                },
                Err(e) => return Err(io::Error::from(io::ErrorKind::BrokenPipe)),
            };
        }
        Ok(())
    }

    fn route(&mut self, (msg, responder): (MapMessage, oneshot::Sender<io::Result<MapMessage>>)) -> io::Result<()> {
        trace!("Routing request to transport");
        match self.transport.start_send((self.next_id, msg)) {
            Ok(AsyncSink::Ready) => (),
            Ok(AsyncSink::NotReady(msg)) => {
                trace!("   --> not ready");

                return Err(io::Error::from(io::ErrorKind::Other));
            },
            Err(e) => return Err(e),
        };

        self.in_flight.insert(self.next_id, responder);
        self.next_id += 1;
        trace!("Next RequestId: {}", self.next_id);
        Ok(())
    }
}

impl<T> Future for Dispatch<T>
    where T: Stream<Item=HtspFrame, Error=io::Error>
           + Sink<SinkItem=(RequestId, MapMessage), SinkError=io::Error>
{
    type Item=();
    type Error=io::Error;

    fn poll(&mut self) -> Poll<Self::Item, Self::Error> {
        trace!("Dispatch::tick");

        let mut work_done = true;

        while work_done {
            work_done = false;
            // TODO: some decent error handling might be useful here,
            //       rather than a lame try!()
            match try!(self.requests.poll()
                       .map_err(|_| io::Error::from(io::ErrorKind::UnexpectedEof))) {
                Async::NotReady => (),
                Async::Ready(Some(request)) => {
                    work_done = true;
                    try!(self.route(request?))
                },
                Async::Ready(None) => {
                    trace!("Service dropped transmission");
                    // TODO: Cleanup transport
                }
            };

            match try!(self.transport.poll_complete()) {
                Async::NotReady => {
                    info!("transport NotReady");
                },
                Async::Ready(()) => {
                    info!("transport Ready");
                },
            }

            match try!(self.transport.poll()) {
                Async::NotReady => (),
                Async::Ready(Some(t)) => {
                    work_done = true;
                    try!(self.dispatch(t))
                },
                Async::Ready(None) => {
                    info!("transport.poll(): Ready. Shutting down");
                    return Ok(Async::Ready(()))
                }
            };

            match self.metadata_requests.poll() {
                Ok(Async::NotReady) => (),
                Ok(Async::Ready(Some(t))) => {
                    work_done = true;
                    info!("dispatch received a metadata subscription");
                    self.metadata_subscriptions.push(t);
                },
                Ok(Async::Ready(None)) => {
                    info!("metadata_requests.poll(): Ready.");
                }
                Err(e) => {
                    error!("metadata_requests.poll() threw an error: {:?}", e);
                }
            }
        }

        Ok(Async::NotReady)
    }
}
