use std::io;

use futures::{BoxFuture, Future, Sink, Stream};
use futures::stream::BoxStream;
use futures::sync::mpsc;

use tokio_core::reactor::Handle;
use tokio_proto::multiplex::RequestId;
use tokio_proto::util::client_proxy::{ClientProxy, Response};
use tokio_io::{AsyncRead, AsyncWrite};
use tokio_io::codec::Framed;
use tokio_service::Service;

use super::proto::{HtspFrame, HtspProto};
use super::codec::HtspCodec;

use ::message::{HtsMsgField, MapMessage};
use ::metadata::Metadata;

pub struct HtspService {
    proxy: ClientProxy<MapMessage, MapMessage, io::Error>,
    metadata_tx: mpsc::Sender<mpsc::Sender<Metadata>>,
}

impl HtspService {
    pub fn new(io: ClientProxy<MapMessage, MapMessage, io::Error>,
               handle: &Handle,
               metadata_tx: mpsc::Sender<mpsc::Sender<Metadata>>) -> Self {
        HtspService {
            proxy: io,
            metadata_tx: metadata_tx,
        }
    }

    pub fn subscribe_metadata(&mut self) -> BoxStream<Metadata, io::Error> {
        let (tx, rx) = mpsc::channel(512);
        // TODO: handle both
        self.metadata_tx.start_send(tx);
        self.metadata_tx.poll_complete();
        rx.map_err(|_| io::Error::from(io::ErrorKind::Interrupted))
            .boxed()
    }
}

impl Service for HtspService {
    type Request = MapMessage;
    type Response = MapMessage;
    type Future = Response<MapMessage, io::Error>;
    type Error = io::Error;

    fn call(&self, req: Self::Request) -> Self::Future {
        self.proxy.call(req)
    }
}
