use std::io;
use std::net::SocketAddr;

use futures::{Stream, Future};
use futures::stream::BoxStream;
use futures::sync::mpsc::Receiver;
use ring::digest;
use tokio_core::reactor::Handle;
use tokio_core::net::TcpStream;
use tokio_proto::TcpClient;
//use tokio_proto::multiplex::ClientService;
use tokio_service::Service;

use ::connection::{HtspProto,HtspService};
use ::message::HtsMsgField;
use ::metadata::Metadata;

struct ServerData {
    version: u32,
    server_name: String,
    server_version: String,
    challenge: Vec<u8>,
    languages: Vec<String>,
    servercapability: Vec<String>,
    //webroot: String,
}

pub struct Connected;
pub struct Negotiated {
    serverdata: ServerData,
}
pub struct Authenticated {
    serverdata: ServerData,
}
#[allow(dead_code)]
enum Authentication {
    None,
    Authenticated,
}

pub enum ConnectionStateWrapper {
    Disconnected,
    Connected(ConnectionStateMachine<Connected>),
    Negotiated(ConnectionStateMachine<Negotiated>),
    Authenticated(ConnectionStateMachine<Authenticated>)
}

pub struct ConnectionStateMachine<S> {
    state: S,
    service: HtspService,
}

impl<S> ConnectionStateMachine<S> {
    fn rpc(&self, method: &str, mut req: ::message::MapMessage) -> Box<Future<Item=::message::MapMessage, Error=io::Error> + Send> {
        req.insert("method".into(), method.to_owned().into());
        self.service.call(req)
            .then(|msg| {
                let msg = msg?;
                if let Some(&HtsMsgField::S64(1)) = msg.get("noaccess".into()) {
                    warn!("Permission denied.");
                    return Err(io::Error::new(io::ErrorKind::PermissionDenied,
                                              "Permission denied on request."))
                }
                if let Some(&HtsMsgField::Str(ref error_msg)) = msg.get("error".into()) {
                    error!("Error occured: {}", error_msg);
                    return Err(io::Error::new(io::ErrorKind::Other,
                                              error_msg.clone()));
                }
                Ok(msg.without("seq"))
            })
        .boxed()
    }
}

impl ConnectionStateMachine<Negotiated> {
    fn authenticate(self, username: &str, password: &str) -> Box<Future<Item=ConnectionStateMachine<Authenticated>, Error=io::Error> + Send> {
        let password = password.to_owned();
        let mut concat = password.into_bytes();
        concat.extend_from_slice(&self.state.serverdata.challenge);
        let digest = digest::digest(&digest::SHA1, &concat);
        info!("Challenge is {} bytes long", self.state.serverdata.challenge.len());
        info!("Digest is {} bytes long", digest.as_ref().len());

        let map = vec![
               ("username".into(), HtsMsgField::Str(username.into())),
               ("digest".into(), HtsMsgField::Bin(Vec::from(digest.as_ref())))].into();

        self.rpc("authenticate", map).and_then(move |msg| {
            info!("Authentication succesful");
            let new_state = ConnectionStateMachine::<Authenticated> {
                state: Authenticated {
                    serverdata: self.state.serverdata
                },
                service: self.service,
            };
            Ok(new_state)
        }).boxed()
    }
}

impl ConnectionStateMachine<Connected> {
    fn new(service: HtspService) -> ConnectionStateMachine<Connected> {
        ConnectionStateMachine::<Connected> {
            state: Connected,
            service: service,
        }
    }

    fn hello(self, client_name: &str, client_version: &str) -> Box<Future<Item=ConnectionStateMachine<Negotiated>, Error=io::Error> + Send> {
        let map = vec![
               ("htspversion".into(), HtsMsgField::S64(25)),
               ("clientname".into(), HtsMsgField::Str(client_name.into())),
               ("clientversion".into(), HtsMsgField::Str(client_version.into()))].into();

        self.rpc("hello", map).and_then(move |msg| {
            let version = try!(msg.get_i64("htspversion")) as u32;
            Ok(ConnectionStateMachine::<Negotiated> {
                service: self.service,
                state: Negotiated {
                    serverdata: ServerData {
                        server_name: try!(msg.get_string("servername")),
                        server_version: try!(msg.get_string("serverversion")),
                        challenge: try!(msg.get_bin("challenge")),
                        languages: vec![], // TODO
                        servercapability: vec![],
                        //webroot: try!(msg.get_string("webroot")),
                        version: version,
                    }
                },
            })
        }).boxed()
    }
}

impl ConnectionStateMachine<Authenticated> {
    pub fn subscribe_metadata(&mut self) -> BoxStream<Metadata, io::Error> {
        // TODO: maybe move this logic up to Dispatch?
        let msg = vec![
            ("epg".into(), HtsMsgField::S64(1)),
        ].into();

        let subscription = self.service.subscribe_metadata();

        self.rpc("enableAsyncMetadata", msg)
        .and_then(move |msg| {
            debug_assert!(msg.is_empty(), "{:?}", msg);
            info!("Got ack for enableAsyncMetadata");
            Ok(subscription)
        })
        .flatten_stream().boxed()
    }
}

impl ConnectionStateWrapper {
    pub fn connect(addr: &SocketAddr, handle: &Handle) -> Box<Future<Item=Self, Error=io::Error>> {
        let ret = TcpClient::new(HtspProto)
            .connect(addr, handle)
            .map(move |service| {
                ConnectionStateWrapper::default(service)
            });
        Box::new(ret)
    }

    pub fn default(service: HtspService) -> ConnectionStateWrapper {
        ConnectionStateWrapper::Connected(ConnectionStateMachine::<Connected>::new(service))
    }

    pub fn hello(self, client_name: &str, client_version: &str) -> Box<Future<Item=Self, Error=io::Error> + Send> {
        match self {
            ConnectionStateWrapper::Connected(state) => {
                state.hello(client_name, client_version).and_then(|new_state| {
                    Ok(ConnectionStateWrapper::Negotiated(new_state))
                }).boxed()
            },
            _ => panic!("Connection already said hello!"),
        }
    }

    pub fn authenticate(self, username: &str, password: &str) -> Box<Future<Item=ConnectionStateMachine<Authenticated>, Error=io::Error> + Send> {
        match self {
            ConnectionStateWrapper::Negotiated(state) => {
                state.authenticate(username, password).then(|result| {
                    match result {
                        Ok(state_machine) => {
                            info!("Authentication succesful");
                            Ok(state_machine)
                        },
                        //Err(ref e) if e.kind() == io::ErrorKind::BrokenPipe || e.kind() == io::ErrorKind::PermissionDenied => {
                        //    error!("Not authenticated, connection terminated");
                        //    Ok(ConnectionStateWrapper::Disconnected)
                        //},
                        Err(other) => Err(other)
                    }
                })
            },
            _ => panic!("Not yet negotiated!")
        }.boxed()
    }

    fn get_negotiated_state(&self) -> Option<&ServerData> {
        match self {
            &ConnectionStateWrapper::Negotiated(ref state) => {
                Some(&state.state.serverdata)
            },
            &ConnectionStateWrapper::Authenticated(ref state) => {
                Some(&state.state.serverdata)
            },
            _ => None
        }
    }
    pub fn version(&self) -> Option<u32> {
        self.get_negotiated_state().map(|s| s.version)
    }
    pub fn server_version(&self) -> Option<&String> {
        self.get_negotiated_state().map(|s| &s.server_version)
    }
    pub fn server_name(&self) -> Option<&String> {
        self.get_negotiated_state().map(|s| &s.server_name)
    }
}
