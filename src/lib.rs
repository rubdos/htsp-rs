#[macro_use]
extern crate futures;
extern crate tokio_core;
extern crate tokio_io;
extern crate tokio_proto;
extern crate tokio_service;
#[macro_use] extern crate log;
extern crate ring;
extern crate bytes;

// Tests

#[cfg(test)]
#[macro_use] extern crate matches;

#[cfg(test)]
extern crate env_logger;

// Import all public modules/structs/Traits

mod connection;
mod message;
pub mod metadata;

pub use connection::Connection;
pub use message::{HtsMsgField,MapMessage};
pub use metadata::{Metadata, MetadataManager, MetadataManagerFuture};
