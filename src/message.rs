use std::io;
use std::collections::HashMap;
use std::iter::FromIterator;

use bytes::{BytesMut, BufMut, ByteOrder, BigEndian, LittleEndian};

// A message can be of either map or list type.
// In a map each field has a name, in a list the members do not have names, but the order should be preserved.
//
// The field types are:
//
// ||Name||ID||Description
// ||Map ||1 ||Sub message of type map
// ||S64 ||2 ||Signed 64bit integer
// ||Str ||3 ||UTF-8 encoded string
// ||Bin ||4 ||Binary blob
// ||List||5 ||Sub message of type list
//
// Taken from https://tvheadend.org/projects/tvheadend/wiki/Htsmsgbinary on Jan 28, 2017

#[derive(Debug)]
pub enum HtsMsgField {
    Map(MapMessage),
    S64(i64),
    Str(String),
    Bin(Vec<u8>),
    List(ListMessage),
}

impl HtsMsgField {
    fn get_type_id(&self) -> u8 {
        match *self {
            HtsMsgField::Map(_) => 1,
            HtsMsgField::S64(_) => 2,
            HtsMsgField::Str(_) => 3,
            HtsMsgField::Bin(_) => 4,
            HtsMsgField::List(_) => 5,
        }
    }
    fn encode(&self, name: &String, buf: &mut BytesMut) -> io::Result<()> {
        buf.reserve(1+1+4+name.as_bytes().len());

        buf.put_u8(self.get_type_id());

        if name.as_bytes().len() > 255 {
            return Err(io::Error::new(io::ErrorKind::InvalidInput,
                                      "Field name too long for field"));
        }
        buf.put_u8(name.as_bytes().len() as u8);
        let length_index = buf.len();

        assert!(buf.remaining_mut() >= 4);

        buf.put_u32::<BigEndian>(0);
        buf.put_slice(&mut name.clone().into_bytes());

        match *self {
            HtsMsgField::Map(ref msg) => {
                trace!("Encoding Map");
                try!(msg.encode(buf));
            },
            HtsMsgField::S64(int) => {
                trace!("Encoding int {}", int);
                buf.reserve(8);
                // TODO: This can be more efficiently encoded.
                buf.put_i64::<LittleEndian>(int);
            },
            HtsMsgField::Str(ref string) => {
                trace!("Encoding string {}", string);
                buf.reserve(string.as_bytes().len());
                let mut a = string.as_bytes();
                buf.put_slice(&mut a);
            },
            HtsMsgField::Bin(ref bin) => {
                buf.extend_from_slice(bin);
            },
            HtsMsgField::List(ref lst) => {},
        };
        let length = buf.len() - (length_index + name.as_bytes().len() + 4); // 4 from length field itself
        BigEndian::write_u32(&mut buf[length_index..length_index+4],
                                length as u32);
        Ok(())
    }
    fn decode(buf: &mut BytesMut) -> io::Result<(String, Self)> {
        if buf.len() < 6 {
            return Err(io::Error::new(io::ErrorKind::UnexpectedEof,
                                      "Not enough bytes to decode a field."))
        }
        let type_id = *&(buf.split_to(1))[0];
        let name_length = *&(buf.split_to(1))[0] as usize;
        let data_length = BigEndian::read_u32(&buf.split_to(4)) as usize;

        let name_buf = buf.split_to(name_length);
        let name = String::from_utf8_lossy(&name_buf);
        trace!("HtsMsgField name '{}' ({} bytes)", name, name_length);

        let mut data_buf = buf.split_to(data_length);

        let field = match type_id {

            // Sub message of type map
            1 => HtsMsgField::Map(
                        try!(MapMessage::decode(&mut data_buf))),

            // Signed 64bit integer
            2 => {
                match data_length {
                    0 => HtsMsgField::S64(0),
                    1 => HtsMsgField::S64(*&data_buf[0] as i64),
                    2 => HtsMsgField::S64(LittleEndian::read_u16(&data_buf) as i64),
                    4 => HtsMsgField::S64(LittleEndian::read_u32(&data_buf) as i64),
                    8 => HtsMsgField::S64(LittleEndian::read_i64(&data_buf)),
                    _ => {
                        error!("BUG: non standard integer length {}", data_length);
                        return Err(io::Error::new(io::ErrorKind::InvalidData,
                                                  "Non standard integer length."));
                    }
                }
            },

            // UTF-8 encoded string
            3 => HtsMsgField::Str(String::from_utf8_lossy(&data_buf).into_owned()),

            // Binary blob
            4 => HtsMsgField::Bin(data_buf.into_iter().collect()),

            // Sub message of type list
            5 => HtsMsgField::List(try!(ListMessage::decode(&mut data_buf))),
            _ => {
                return Err(io::Error::new(io::ErrorKind::InvalidData,
                                          "Invalid/unimplemented MsgField type"));
            }
        };
        Ok((name.into_owned(), field))
    }
}


impl From<i64> for HtsMsgField {
    fn from(st: i64) -> HtsMsgField {
        HtsMsgField::S64(st)
    }
}

impl From<String> for HtsMsgField {
    fn from(st: String) -> HtsMsgField {
        HtsMsgField::Str(st)
    }
}

#[derive(Debug)]
pub struct MapMessage {
    fields: HashMap<String, HtsMsgField>,
}

struct FieldExtractor<'a> {
    buf: &'a mut BytesMut,
}

impl<'a> FieldExtractor<'a> {
    fn extract(buf: &'a mut BytesMut) -> FieldExtractor<'a> {
        FieldExtractor {
            buf: buf,
        }
    }
}
impl<'a> Iterator for FieldExtractor<'a> {
    type Item = io::Result<(String, HtsMsgField)>;

    fn next(&mut self) -> Option<Self::Item> {
        // As long as we have data in the intermediate buffer
        if self.buf.len() == 0 {
            return None;
        }
        trace!("Decoding message with {} bytes", self.buf.len());
        match HtsMsgField::decode(self.buf) {
            Ok((name, val)) => Some(Ok((name, val))),
            Err(error) => Some(Err(error)),
        }
    }
}

impl MapMessage {
    pub fn decode(buf: &mut BytesMut) -> io::Result<Self> {
        let extractor = FieldExtractor::extract(buf);
        let items: io::Result<Vec<_>> = extractor.collect();
        let fields = HashMap::from_iter(try!(items));

        Ok(MapMessage{
            fields: fields,
        })
    }
    pub fn encode(&self, buf: &mut BytesMut) -> io::Result<()> {
        for (name, value) in &self.fields {
            try!(value.encode(name, buf));
        }
        trace!("Encoded map msg");
        Ok(())
    }

    pub fn insert(&mut self, key: String, val: HtsMsgField) {
        self.fields.insert(key, val);
    }
    pub fn get(&self, key: &str) -> Option<&HtsMsgField> {
        self.fields.get(key.into())
    }
    pub fn get_i64(&self, key: &str) -> io::Result<i64> {
        match self.get(key) {
            Some(&HtsMsgField::S64(field)) => Ok(field),
            None => Err(io::Error::new(io::ErrorKind::NotFound,
                                       "Key for int64 not in message")),
            _ => Err(io::Error::new(io::ErrorKind::InvalidData,
                                   "Field is not of type S64"))
        }
    }
    pub fn get_string(&self, key: &str) -> io::Result<String> {
        match self.get(key) {
            Some(&HtsMsgField::Str(ref field)) => Ok(field.clone()),
            None => Err(io::Error::new(io::ErrorKind::NotFound,
                                       "Key for string not in message")),
            _ => Err(io::Error::new(io::ErrorKind::InvalidData,
                                   "Field is not of type string"))
        }
    }
    pub fn get_bin(&self, key: &str) -> io::Result<Vec<u8>> {
        match self.get(key) {
            Some(&HtsMsgField::Bin(ref field)) => Ok(field.clone()),
            None => Err(io::Error::new(io::ErrorKind::NotFound,
                                       "Key for bin not in message")),
            _ => Err(io::Error::new(io::ErrorKind::InvalidData,
                                   "Field is not of type string"))
        }
    }
    pub fn is_empty(&self) -> bool {
        self.fields.is_empty()
    }

    pub fn without<S: AsRef<str>>(mut self, key: S) -> Self{
        self.fields.remove(key.as_ref().into());
        self
    }
}

#[allow(dead_code)]
#[derive(Debug)]
pub struct ListMessage {
    fields: Vec<HtsMsgField>
}

impl ListMessage {
    fn decode(buf: &mut BytesMut) -> io::Result<Self> {
        let remove_names = |val: (String, HtsMsgField)| -> io::Result<HtsMsgField> {
            let (name, value) = val;
            if name.len() > 0 {
                Err(io::Error::new(io::ErrorKind::InvalidData,
                                   "ListMessage type cannot have a name for a field."))
            } else {
                Ok(value)
            }
        };
        let extractor = FieldExtractor::extract(buf);
        let items: Vec<(String, HtsMsgField)> = try!(io::Result::from_iter(extractor));
        let fields = io::Result::from_iter(items.into_iter().map(remove_names));

        Ok(ListMessage{
            fields: fields?,
        })
    }
}

impl From<Vec<(String, HtsMsgField)>> for MapMessage {
    fn from(map: Vec<(String, HtsMsgField)>) -> MapMessage {
        MapMessage {
            fields: map.into_iter().collect(),
        }
    }
}

#[cfg(test)]
#[test]
fn map_encode() {
    let mut fields = HashMap::new();
    fields.insert(String::from("test"), HtsMsgField::S64(-1));
    let msg = MapMessage {
        fields: fields,
    };
    let mut encoded = BytesMut::new();
    msg.encode(&mut encoded).expect("Could not encode a simple map");
    assert_eq!(encoded.len(), 6+4+8);

    for i in encoded.iter() {
        println!("{}", i);
    }

    let mut buf = BytesMut::from(encoded);
    MapMessage::decode(&mut buf).unwrap();
}

#[cfg(test)]
#[test]
fn test_s64() {
    let pairs = vec![
        (vec![2u8, 1u8, 0u8, 0u8, 0u8, 1u8, 97u8, 9u8], 9),
        (vec![2u8, 1u8, 0u8, 0u8, 0u8, 2u8, 97u8, 0u8, 1u8], 256),
        (vec![2u8, 1u8, 0u8, 0u8, 0u8, 8u8, 97u8, 255u8, 255u8, 255u8, 255u8, 255u8, 255u8, 255u8, 255u8], -1),
    ];
    for (data, result) in pairs {
        let mut buf = BytesMut::from(data);
        let (name, field) = HtsMsgField::decode(&mut buf).unwrap();
        assert!(name == "a");
        match field {
            HtsMsgField::S64(r) => assert_eq!(r, result),
            _ => unreachable!()
        }
    }
}

#[cfg(test)]
#[test]
fn test_complex_field_names() {
    let pairs = vec![
        (vec![2u8, 3u8, 0u8, 0u8, 0u8, 1u8, 0xE2u8, 0x98u8, 0xBAu8, 9u8], "☺"),
        (vec![2u8, 6u8, 0u8, 0u8, 0u8, 1u8, 0xE2u8, 0x98u8, 0xBBu8, 0xE2u8, 0x9Cu8, 0x8Cu8, 9u8], "☻✌"),
    ];
    for (data, result) in pairs {
        let mut buf = BytesMut::from(data);
        let (name, _) = HtsMsgField::decode(&mut buf).unwrap();
        assert!(name == result);
    }

    // Try encoding some too.
    let mut fields = HashMap::new();
    fields.insert(String::from("☺"), HtsMsgField::S64(-1));
    let msg = MapMessage {
        fields: fields,
    };
    let mut encoded = BytesMut::new();
    msg.encode(&mut encoded).expect("Could not encode emoji as key for a map.");
    let mut buf = BytesMut::from(encoded);
    let decoded = MapMessage::decode(&mut buf).expect("Could not decode encoded emoji in key.");
    assert!(decoded.fields.contains_key("☺"));
    match decoded.fields["☺"] {
        HtsMsgField::S64(int) => assert_eq!(int, -1),
        _ => unreachable!()
    }
}

#[cfg(test)]
#[test]
fn simple_decode() {
    // The shortest message possible.
    let mut b = BytesMut::from(vec![/*0u8, 0u8, 0u8, 0u8*/]);
    let msg = MapMessage::decode(&mut b);
    assert!(msg.is_ok(), "{}", msg.err().unwrap());

    // The second shortest message possible:
    // A message { "a" => 7 }
    let mut b = BytesMut::from(vec![//0u8, 0u8, 0u8, 8u8,
                                   2u8, 1u8, 0u8, 0u8, 0u8, 1u8,
                                  97u8, 7u8]);
    let msg = MapMessage::decode(&mut b).unwrap();
    assert!(matches!(msg.get("a".into()), Some(&HtsMsgField::S64(7))));
}

#[cfg(test)]
#[test]
fn test_nested_map() {
    let mut b = BytesMut::from(vec![// 0u8, 0u8, 0u8, 7u8,
                                    1u8, 1u8, 0u8, 0u8, 0u8, 0u8,
                                   97u8,
                                    /*empty submap*/]);
    let msg = MapMessage::decode(&mut b).unwrap();
    match msg.get("a".into()) {
        Some(&HtsMsgField::Map(ref submap)) => {
            assert!(submap.is_empty());
        },
        _ => unreachable!()
    }
}
