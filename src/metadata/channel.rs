use std::io;

use ::message::MapMessage;

// channelId          u32   required   ID of channel.
// channelNumber      u32   required   Channel number, 0 means unconfigured.
// channelNumberMinor u32   optional   Minor channel number (Added in version 13).
// channelName        str   required   Name of channel.
// channelIcon        str   optional   URL to an icon representative for the channel
//                                     (For v8+ clients this could be a relative /imagecache/ID URL
//                                      intended to be fed to fileOpen() or HTTP server)
//                                     (For v15+ clients this could be a relative imagecache/ID URL
//                                      intended to be fed to fileOpen() or HTTP server)
// eventId            u32   optional   ID of the current event on this channel.
// nextEventId        u32   optional   ID of the next event on the channel.
// tags               u32[] optional   Tags this channel is mapped to.
// services           msg[] optional   List of available services (Added in version 5)
#[derive(Clone,Eq,PartialEq,Debug)]
pub struct Channel {
    pub id: u32,
    pub number: u32,
    pub number_minor: Option<u32>,
    pub name: String,
    pub icon: Option<String>,
    pub event_id: Option<u32>,
    pub next_event_id: Option<u32>,
    pub tags: Option<Vec<u32>>,
    pub services: Option<Vec<()>>,
}

impl From<MapMessage> for io::Result<Channel> {
    fn from(msg: MapMessage) -> io::Result<Channel> {
        Ok(Channel {
            id: try!(msg.get_i64("channelId")) as u32,
            number: try!(msg.get_i64("channelNumber")) as u32,
            number_minor: msg.get_i64("channelNumberMinor").ok().map(|a| a as u32),
            name: try!(msg.get_string("channelName")),
            icon: msg.get_string("channelIcon").ok(),
            event_id: msg.get_i64("eventId").ok().map(|a| a as u32),
            next_event_id: msg.get_i64("next_eventId").ok().map(|a| a as u32),
            tags: None, //TODO
            services: None, //TODO
        })
    }
}
