use std::{self,io};

use futures::{Async, BoxFuture, Stream, Future, Poll};

use message::{MapMessage,HtsMsgField};

mod channel;
pub use self::channel::Channel;

#[derive(Clone,PartialEq,Eq,Debug)]
pub enum Metadata {
    TagAdd,
    TagUpdate,
    TagDelete,
    ChannelAdd(Channel),
    ChannelUpdate,
    ChannelDelete,
    DvrEntryAdd,
    DvrEntryUpdate,
    DvrEntryDelete,
    EventAdd,
    EventUpdate,
    EventDelete,
    InitialSyncCompleted,
}

impl From<MapMessage> for io::Result<Metadata> {
    fn from(msg: MapMessage) -> io::Result<Metadata> {
        use self::Metadata::*;

        let method = match msg.get("method").expect("No method") {
            &HtsMsgField::Str(ref method) => method,
            _ => unreachable!("No string method"),
        }.clone();

        Ok(match method.as_ref() {
            "tagAdd" => TagAdd,
            "tagUpdate" => TagUpdate,
            "tagDelete" => TagDelete,
            "channelAdd" => ChannelAdd(try!(msg.into())),
            "channelUpdate" => ChannelUpdate,
            "channelDelete" => ChannelDelete,
            "dvrEntryAdd" => DvrEntryAdd,
            "dvrEntryUpdate" => DvrEntryUpdate,
            "dvrEntryDelete" => DvrEntryDelete,
            "eventAdd" => EventAdd,
            "eventUpdate" => EventUpdate,
            "eventDelete" => EventDelete,
            "initialSyncCompleted" => InitialSyncCompleted,
            _ => unreachable!("Invalid method {}", method),
        })
    }
}

pub struct MetadataManagerFuture<T> {
    stream: Option<T>,
    data: Vec<Metadata>,
}

impl<T> MetadataManagerFuture<T> {
    fn process_metadata(&mut self, data: Metadata) -> Async<()> {
        match data {
            Metadata::InitialSyncCompleted => return Async::Ready(()),
            data => self.data.push(data),
        }
        Async::NotReady
    }
}

impl<T> From<T> for MetadataManagerFuture<T>
    where T: Stream<Item=Metadata,Error=io::Error>
{
    fn from(stream: T) -> MetadataManagerFuture<T> {
        MetadataManagerFuture {
            stream: Some(stream),
            data: vec![],
        }
    }
}

impl<T> Future for MetadataManagerFuture<T>
    where T: Stream<Item=Metadata,Error=io::Error>
{
    type Item = (MetadataManager, T);
    type Error = T::Error;

    fn poll(&mut self) -> Poll<Self::Item, Self::Error> {
        trace!("MetadataManagerFuture::poll");

        loop {
            match try!(self.stream.as_mut().unwrap().poll()) {
                Async::NotReady => return Ok(Async::NotReady),
                Async::Ready(Some(m)) => match self.process_metadata(m) {
                    Async::Ready(_) => {
                        return Ok(Async::Ready((MetadataManager::populate(self.data.drain(..)), self.stream.take().unwrap())))
                    }
                    _ => ()
                },
                Async::Ready(None) => {
                    error!("Metadata stream dropped transmission");
                    return Err(io::Error::from(io::ErrorKind::Interrupted))
                }
            };
        }
    }
}

pub struct MetadataManager {
    pub channels: Vec<Channel>,
}

impl MetadataManager {
    fn populate<T>(population: T) -> MetadataManager
        where T: Iterator<Item=Metadata>
    {
        // TODO: we might want this "sorting" to have happened while retrieving the data.
        info!("Populating new MetadataManager");
        let mut manager = MetadataManager {
            channels: vec![],
        };
        for meta in population {
            manager.update(meta);
        }
        manager
    }

    pub fn update(&mut self, meta: Metadata) {
        match meta {
            Metadata::ChannelAdd(channel) => {
                self.channels.push(channel);
                //info!("Indexing channel {:?}", channel);
            }
            _ => {
                error!("Metadata {:?} not implemented", meta);
            }
        }
    }
}
